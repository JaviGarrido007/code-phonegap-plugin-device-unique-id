//
//  CDVUniqueDeviceID.h
//
//
//
    
#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface CDVUniqueDeviceID : CDVPlugin

- (void)get:(CDVInvokedUrlCommand*)command;

- (void)get_code:(CDVInvokedUrlCommand*)command;

@end